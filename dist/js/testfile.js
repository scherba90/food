'use strict';

/* function showThis(a, b) {
console.log(this);
	function sum() {
		return a + b;
	}
    console.log(sum());
}

showThis(4, 5); */




/* const obj = {
	a: 20,
	b: 15,
	sum: function () {
		function shout() {
			console.log(this)
		}
		shout()
	}
}
obj.sum(); */

/* function User(name, id) {
	this.name = name;
	this.id = id;
	this.human = true;
  }
  let ivan = new User('Ivan', 23) */




/* function sayName(surname) {
  console.log(this)
  console.log(this.name + surname)
}

const user = {
	name: 'John'
}

sayName.call(user, 'Smith')
sayName.apply(user, ['Smith'])

function count(num) {
return this*num
}

const double = count.bind(2)
console.log(double(3))
console.log(double(13)) */


/* const btn = document.querySelector('button');

btn.addEventListener('click', (e) =>{
	e.target.style.backgroundColor = 'red';
  });

const obj = {
  num: 5,
  sayNumber: function () {
	  const say = () => {
		  console.log(this.num);
	  }
	  say();
    }
}
obj.sayNumber()




const double = (a, b) => a * 2
console.log(double(4)) */

// const log = function (a, b, ...rest) {
// 	console.log(a, b, rest)
//   }

// log('basic', 'rest', 'operator', 'usage')

// function calc0rDouble(number, basis = 2) {
// 	console.log(number * basis)
// }
// calc0rDouble(3)

//filter

// const names = ['ivan', 'ann', 'ksenis', 'voldermart']

// const shortNames = names.filter(function (name) {
// 	return name.length < 5;
//   })

// console.log(shortNames)

//map

//let answers = ['Ivan', 'AnnA', 'Hello'];
//
//answers = answers.map(item => item.toLowerCase());
//
//console.log(answers) 

//every/some

// const some = [4, 'qwq', 'afwef'];

// console.log(some.some(item => typeof(item) === 'number'))

// console.log(some.every(item => typeof(item) === 'number'))

//reduce

/* const arr = [4, 5, 1, 3, 2, 6]


const res = arr.reduce((sum, current) => sum + current, 3)
console.log(res)
  */

/* const arr = ['apple', 'pear', 'plum']


const res = arr.reduce((sum, current) => `${sum}, ${current}`)
console.log(res)
 */

// const obj = {
// 	ivan: 'persone',
// 	ann: 'persone',
// 	dog: 'animal',
// 	cat: 'animal'
// }

// const newArr = Object.entries(obj)
// .filter(item => item[1] === 'persone')
// .map(item => item[0])

// console.log(newArr)

// json-server db.json - старт сервера
 
       /* Геттеры и сеттеры */

/* const persone = {
	name: "Alex",
	age: 25,

	get userAge() {
		return this.age;
			},

	set userAge(num) {
		this.age = num;
			},
};

console.log(persone.userAge = 30)
console.log(persone.userAge) */

/* Инкапсуляция */

class User {
	constructor(name, age) {
        this.name = name
        this._age = age
	}

	#surname = 'Shcherbakov';


		say() {
			console.log(`Имя пользователя: ${this.name} ${this.#surname}, возраст ${this._age}`)
		}

		get age() {
			return this._age
		}

		set age(age) {
			if (typeof age === 'number' && age > 0 && age < 110) {
				this._age = age
			} else {
				console.log('Недопустимое значение!');
			}
		}
	
}

  const dima = new User('Dima', 27)
  console.log(dima.surname)
  dima.say();