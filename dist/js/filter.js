'use strict';

//filter

// const names = ['Ivan', 'Ann', 'Ksenia', 'Volderamt']
// 
// const shortNames = names.filter(function(name) {
//     return name.length < 5
// })
// 
// console.log(shortNames)

//map 
 
//const answers = ['IvAn', 'AnnA', 'KsEnia', 'Volderamt']
//
//const result = answers.map(item => item.toLowerCase())
//
//console.log(result)

// every/some

//const some = [4, 2, 8]

/* console.log(some.some(item => typeof(item) === 'number')) */

//console.log(some.every(item => typeof(item) === 'number'))

// reduce

/* const arr = [4 ,5, 1, 3, 2, 6]

const res = arr.reduce((sum, current) => sum + current)

console.log(res) */

/* const arr = ['asd', 'sad', 'asdfw']

const res = arr.reduce((sum, current) => `${sum}, ${current}`)

console.log(res) */

const obj = {
    ivan: 'persone',
    ann: 'persone',
    dog: 'animal',
    cat: 'animal'
}

const newArr = Object.entries(obj)
.filter(item => item[1]  === 'persone')
.map(item => item[0])

console.log(newArr)

