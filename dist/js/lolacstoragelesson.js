'use strict'

// localStorage.setItem('number', 5) //записать ключ(название ключа, значение ключа)
// localStorage.getItem('number') //получить данные()
// localStorage.removeItem('number') //удалить ключ
// localStorage.clear('number') //очисить localStorage хранилище
// 
// console.log(localStorage.getItem('number'))

const checkbox = document.querySelector('#checkbox'),
          form = document.querySelector('form'),
change = document.querySelector('#color')
        
if (localStorage.getItem('isChecked')) {
    checkbox.checked = true;
}

if (localStorage.getItem('bg') === 'changed') {
    localStorage.removeItem('bg')
    form.style.backgroundColor = '#fff'
} 

checkbox.addEventListener('change', () => {
    localStorage.setItem('isChecked', true)
})

change.addEventListener('click', () => {
    if (localStorage.getItem('bg') === 'changed') {
        localStorage.removeItem('bg')
        form.style.backgroundColor = '#fff'
    } else {
        localStorage.setItem('bg', 'changed')
        form.style.backgroundColor = 'red'
    }
})